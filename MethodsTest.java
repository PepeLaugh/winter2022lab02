public class MethodsTest
{
	public static void main(String[] args)
	{
		int x = 10;
		System.out.println(x);
		methodNoInputNoReturn();
		System.out.println(x);
		methodOneInputNoReturn(10);
		methodOneInputNoReturn(x);
		methodOneInputNoReturn(x+50);
		methodTwoInputNoReturn(12, 11.5);
		int z = methodNoInputReturnInt();
		System.out.println(z);
		double y = sumSquareRoot(6,3); 
		System.out.println(y);
		String s1 = "hello";
		String s2 = "goodbye";
		System.out.println(s1.length());
		System.out.println(s2.length());
		int one = secondClass.addOne(50);
		System.out.println(one);
		secondClass sc = new secondClass();
		System.out.println(sc.addTwo(50));
	}
	
	public static void methodNoInputNoReturn()
	{
		int x = 50;
		System.out.println("Im in a method that takes no input and returns nothing");
		System.out.println(x);
	}
	public static void methodOneInputNoReturn(int a)
	{
		System.out.println("Inside the method one input no return");
		System.out.println(a);
	}
	public static void methodTwoInputNoReturn(int b, double c)
	{
		System.out.println(b);
		System.out.println(c);
	}
	public static int methodNoInputReturnInt()
	{
		int x = 6;
		return x;
	}
	public static double sumSquareRoot(int d, int e) 
	{
		double x = d+e;
		double result = Math.sqrt(x);
		return result;
	}
}