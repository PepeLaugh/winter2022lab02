import java.util.Scanner;

public class PartThree
{
    public static void main(String[] args)
    {
        Scanner scan = new Scanner(System.in);
        double one = scan.nextDouble();
        double two = scan.nextDouble();
        double three = scan.nextDouble();
        double areaSquare = AreaComputations.areaSquare(one);
        AreaComputations sc = new AreaComputations();
        System.out.println(areaSquare);
        System.out.println(sc.areaRectangle(two,three));
    }
}